buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:8.0.2")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.Kotlin.core}")
        classpath("com.google.dagger:hilt-android-gradle-plugin:${Versions.Dagger.core}")
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.AndroidX.Navigation.core}")
    }
}

plugins {
  id ("org.sonarqube") version "4.2.1.3168"
}

sonar {
  properties {
    property("sonar.projectKey", "vedanshgaur_sdmaid-se_AYq3Nz0yb6o9y7ddZtKP")
    property("sonar.projectName", "Sdmaid Se")
    property("sonar.qualitygate.wait", true)
  }
}

allprojects {
    repositories {
        google()
        mavenCentral()
        maven { setUrl("https://jitpack.io") }
    }
}

tasks.register("clean").configure {
    delete("build")
}

